(function($){
    $(document).on('click', '#editable-table td:not(.edit)', function(e) {
        e.preventDefault();

        $(this).addClass('edit');

        var self = $(this),
            id = self.data('id'),
            value = self.text();

        self.html('<input type="text" data-id="' + id + '" value="' + value + '" data-value="' + value + '" />');

        // focus new input element
        var inputElement = $(self.find('input')).get(0),
            elemLen = inputElement.value.length;
        inputElement.selectionStart = elemLen;
        inputElement.selectionEnd = elemLen;
        inputElement.focus();
    });

    function saveData()
    {
        var self = $(this),
            value = self.val(),
            cellContainer = self.parent(),
            oldValue = cellContainer.data('value'),
            id = cellContainer.data('id'),
            is_person = cellContainer.data('is-person');

        if (value != oldValue) {
            $.ajax({
                type: 'POST',
                data: {'id': id, 'value': value, 'is_person': is_person},
                url: $('#editable-table').data('url'),
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        $('#messages').remove();
                        cellContainer.html(value);
                        cellContainer.removeClass('edit');
                    } else {
                        if (response.errors) {
                            $('#messages').remove();

                            var errorHtml = '';
                            errorHtml += '<div id=\"messages\"><div class=\"section clearfix\">';
                            $.each(response.errors, function(key, val) {
                                errorHtml += '<div class=\"messages error\">';
                                errorHtml += '<h2 class=\"element-invisible\">Error message</h2>';
                                errorHtml += val;
                                errorHtml += '</div>';
                            });
                            errorHtml += '</div></div>';

                            $('#header').after(errorHtml);
                        }
                    }
                }
            });
        } else {
            $('#messages').remove();
            cellContainer.html(value);
            cellContainer.removeClass('edit');
        }
    }

    $(document).on('blur', '.edit input', function (e) {
        saveData.call(this);
    });

    $(document).on('keydown', '.edit input', function (e) {
        if (e.which == 13) {
            saveData.call(this);

            return false;
        }

        if (e.which == 27) {
            $(this).val($(this).data('value'));
            saveData.call(this);

            return false;
        }
    });
}(jQuery));
