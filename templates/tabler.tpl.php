<table class="sticky-enabled tableheader-processed sticky-table" <?php foreach ($attrs as $key => $attribute): ?><?php echo $key?>="<?php echo $attribute?>"<?php endforeach ?>>
  <thead>
      <tr>
        <?php foreach ($header as $headerItem): ?>
          <th><?php echo $headerItem['data']; ?></th>
        <?php endforeach; ?>
      </tr>
  </thead>
  <tbody>
  <?php foreach ($rows as $row): ?>
  <tr>
    <?php foreach ($row as $item): ?>
        <td data-id="<?php echo $item['id']; ?>" data-is-person="<?php echo $item['is_person']; ?>" data-value="<?php echo $item['data']; ?>"><?php echo $item['data']; ?></td>
    <?php endforeach; ?>
  </tr>
  <?php endforeach; ?>
  </tbody>
</table>
